# egg-crawler

[![NPM version][npm-image]][npm-url]
[![build status][travis-image]][travis-url]
[![Test coverage][codecov-image]][codecov-url]
[![David deps][david-image]][david-url]
[![Known Vulnerabilities][snyk-image]][snyk-url]
[![npm download][download-image]][download-url]

[npm-image]: https://img.shields.io/npm/v/egg-crawler.svg?style=flat-square
[npm-url]: https://npmjs.org/package/egg-crawler
[travis-image]: https://img.shields.io/travis/eggjs/egg-crawler.svg?style=flat-square
[travis-url]: https://travis-ci.org/eggjs/egg-crawler
[codecov-image]: https://img.shields.io/codecov/c/github/eggjs/egg-crawler.svg?style=flat-square
[codecov-url]: https://codecov.io/github/eggjs/egg-crawler?branch=master
[david-image]: https://img.shields.io/david/eggjs/egg-crawler.svg?style=flat-square
[david-url]: https://david-dm.org/eggjs/egg-crawler
[snyk-image]: https://snyk.io/test/npm/egg-crawler/badge.svg?style=flat-square
[snyk-url]: https://snyk.io/test/npm/egg-crawler
[download-image]: https://img.shields.io/npm/dm/egg-crawler.svg?style=flat-square
[download-url]: https://npmjs.org/package/egg-crawler

Crawler tool for egg.

## Install

```bash
$ npm i egg-crawler --save

or

$ yarn add egg-crawler
```

## Usage

```js
// {app_root}/config/plugin.js
exports.crawler = {
  enable: true,
  package: 'egg-crawler',
};
```

## Example

Create a crawler
```js
// {app_root}/crawler/profile.js
import { Crawler } from 'egg-crawler';

class ProfileCrawler extends Crawler {
  // class instantiation name
  name: string = 'profile';

  async run() {
    // this.scraper -> is scrape-it instantiation
    // const { app, ctx } = this -> that can use every ctx module

    const data = await this.scraper('https://ionicabizau.net', {
      title: '.header h1',
      desc: '.header h2',
      avatar: {
        selector: '.header img',
        attr: 'src',
      },
    });

    return data;
  }
}
```

Run it
```js
// {app_root}/controller/profile.js
import { Controller } from 'egg';

class ProfileController extends Controller {
  async info() {
    const { ctx, app } = this;
    const data = await app.crawler.profile.run();

    ctx.body = data;
  }
}
```

## Reference
- [egg.js](https://github.com/eggjs/egg)
- [scrape-it](https://www.npmjs.com/package/scrape-it)

## License

[MIT](LICENSE)
